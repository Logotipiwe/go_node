import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from "mobx-react";
import RootStore from "./stores/RootStore";

ReactDOM.render(
  <Provider RootStore={new RootStore()}>
    <React.StrictMode>
      <App/>
    </React.StrictMode>
  </Provider>
  ,
  document.getElementById('root')
);

