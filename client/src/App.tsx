import React from 'react';
import './App.scss';
import {inject, observer} from "mobx-react";
import RootStore from "./stores/RootStore";

@inject("RootStore")
@observer
class App extends React.Component<{ RootStore?: RootStore }, any> {
	componentDidMount(): void {
		if (process.env.NODE_ENV === "development") {
			// const RootStore = this.props.RootStore!;
			// RootStore.joinGame(null,'aaaa', 'Gerr')
		}
	}

	render() {
		const RootStore = this.props.RootStore!;
		return (
			<div className="App">
				<header className="App-header">
					{RootStore.appState === "DEFAULT" &&
					<>
						<input placeholder='Ваше имя...' value={RootStore.playerName} onChange={RootStore.inputPlayerName}/>
						<button onClick={RootStore.createGame}>Создать игру</button>
						<div>
							<input
								placeholder='Код игры...'
								onChange={RootStore.onInputHash}
								value={RootStore.inputHash}
							/>
							<button onClick={RootStore.joinGame}>Войти в игру</button>
						</div>
					</>
					}
					{RootStore.appState === "WAITING" &&
					<>
						<p>{RootStore.gameHash}</p>
						<p>Ожидание игрока....</p>
					</>
					}
					{RootStore.appState === "STARTED" &&
					<>
						{RootStore.appData.playerTurnName &&
							<div className='current_turn'>Ход: {RootStore.appData.playerTurnName}</div>
						}
						<div className='player'>
							<p>{RootStore.opponent.name}</p>
						</div>
						<div id='deck'>
							{RootStore.appData.deck && RootStore.appData.deck.map((row, rowNum) =>
								<div className='row'>
									{row.map((cell, cellNum) =>
										<div
											className='cell'
											onClick={RootStore.cellClick.bind(null, rowNum, cellNum)}
										>
											{(cell.player !== null) && <div className={(cell.player === RootStore.playerHash) ? 'chip white' : 'chip black'}/>
											}
										</div>
									)}
								</div>
							)}
						</div>
						<div className='player'>
							<p>{RootStore.you.name}</p>
						</div>
					</>
					}
				</header>
			</div>
		);
	}
}

export default App;

