import {action, computed, observable} from "mobx";
import md5 from 'md5';

export default class RootStore {
	url = (false) ? 'https://3cf4e2e11b28.eu.ngrok.io' : 'http://localhost:3000';
	@observable appState: appStates = "DEFAULT";
	@observable playerName: string = (true) ? '' : md5((Math.random() * 1000).toString()).toString().substr(0, 2);
	@observable title = 'hi';
	@observable inputHash: string = '';
	@observable gameHash: undefined | string = undefined;
	@observable playerHash: undefined | string = undefined;
	@observable appData: any = {};
	@observable poolRequestDelay: number = 100;
	@observable poolErrs = 0;
	@observable errMsg = '';
	@observable elements = ['#F00', '#f80', '#FF0', '#0F0', '#66F'].map((color, id) => ({id, color}));
	poolTimeout: any;

	@computed get you() {
		const players = this.appData.players;
		if (!players) throw new Error('players in appData missing');
		if (!this.playerHash) throw new Error('player hash missing');
		return players[this.playerHash];
	}

	@computed get opponent() {
		const players = this.appData.players;
		if (!players || Object.keys(players).length < 2) throw new Error('players in appData missing');
		if (!this.playerHash) throw new Error('player hash missing');
		return players[
			Object.keys(players).filter(hash => (hash !== this.playerHash))[0]
			];
	}

	@action.bound inputPlayerName(e: any) {
		this.playerName = e.target.value;
	}

	@action.bound doAjax(route) {
		return fetch(this.url + route).then(res => {
			if (res.status === 500) {
				res.json().then(res => {
					clearTimeout(this.poolTimeout);
					throw new Error(res.message);
				})
			}
			return res.json()
		})
	}

	@action.bound createGame() {
		this.doAjax('/game/create' + this.objToGet({
			playerName: this.playerName,
			deckSize: 10
		}))
			.then(res => {
				this.gameHash = res.gameHash;
				this.playerHash = res.playerHash;
				this.appState = "WAITING";
				this.poolRequest();
			});
	}

	@action.bound joinGame(sooqa: any, hash: string | undefined = undefined, name: string | undefined = undefined) {
		const get = {gameHash: hash || this.inputHash, playerName: name || this.playerName};
		this.doAjax('/game/join' + this.objToGet(get))
			.then(res => {
				try {
					this.gameHash = res.gameHash;
					this.playerHash = res.playerHash;
					this.handlePoolRequest(res);
				} catch (e) {
				}
			});
	}

	@action.bound poolRequest() {
		const get = this.objToGet({
			playerHash: this.playerHash,
			gameHash: this.gameHash
		});
		this.doAjax('/game/pooling' + get)
			.then(this.handlePoolRequest)
			.catch(() => {
				this.poolErrs++;
				this.poolRequestDelay = this.poolErrs * 500;
				this.poolTimeout = setTimeout(this.poolRequest, this.poolRequestDelay);
			});
	}

	@action.bound handlePoolRequest(res: any) {
		try {
			this.poolErrs = 0;
			this.appData = Object.assign(this.appData, res.payload);
			if (res.type === "secPlayerJoined") this.appState = "STARTED";
			this.poolRequest();
		} catch (e) {
		}
	}

	@action.bound cellClick(rowNum, cellNum) {
		const get = {
			playerHash: this.playerHash,
			gameHash: this.gameHash,
			rowNum,
			cellNum
		};
		this.doAjax('/game/cellClick' + this.objToGet(get));
	}


	@action.bound onInputHash(e: any) {
		this.inputHash = e.target.value;
	}

	objToGet(obj: any) {
		return '?' + Object.keys(obj).map(key => key + '=' + obj[key]).join('&');
	}
}
