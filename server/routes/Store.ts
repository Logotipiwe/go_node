import md5 from "md5";
import Game from "./Game";
import {Request, Response} from "express";
import Player from "../models/Player";

class Store {
	constructor() {
		this.newGame = this.newGame.bind(this);
		this.joinGame = this.joinGame.bind(this);
		this.generateGameHash = this.generateGameHash.bind(this);
		this.getGame = this.getGame.bind(this);
		this.poolRequest = this.poolRequest.bind(this);
		this.checkGameHash = this.checkGameHash.bind(this);
		this.checkPlayerHash = this.checkPlayerHash.bind(this);
		this.cellClick = this.cellClick.bind(this);


		if (this.test) {
			this.gamesList = {
				'aaaa': new Game('серв', 10, 'aaaa')
			}
		}
	}
	test: boolean = false;
	testIsChangeTurn = true;
	testHash: string = 'hash';
	gamesList: {[key: string]: Game} = {};

	getGame(hash: string){
		return this.gamesList[hash];
	}

	checkGameHash(req: Request, res: Response, next){
			if(
				typeof req.query.gameHash !== 'string' ||
				!this.gamesList[req.query.gameHash]
			) {
				res.status(403);
				throw new Error("hashes check err");
			} else {
				next();
			}
	}
	checkPlayerHash(req: Request, res: Response, next){
		if(
			typeof req.query.gameHash !== 'string' ||
			typeof req.query.playerHash !== 'string' ||
			!this.gamesList[req.query.gameHash] ||
			!this.gamesList[req.query.gameHash].players[req.query.playerHash]
		) {
			res.status(403);
			throw new Error("hashes check err");
		} else {
			next();
		}
	}


	newGame(playerName: string, deckSize: number){
		const gameHash = this.generateGameHash();
		const game = new Game(playerName, deckSize, gameHash);
		this.gamesList[gameHash] = game;
		console.log(this.gamesList);

		return {gameHash, playerHash: Object.keys(game.players)[0]};
	}

	joinGame(req: Request, res: Response, next) {
		if(typeof req.query.gameHash !== "string") throw new Error('gameHash required');
		const game = this.getGame(req.query.gameHash);

		if(!game) next(new Error("missing game"));

		game.join(req, res, next);
	}

	generateGameHash() {
		return md5((Math.random() * 100000).toString()).toString().substr(0, 4);
	}

	poolRequest(req: Request, res: Response) {
		const game = this.getGame((req.query as any).gameHash);
		const player = game.players[(req.query as any).playerHash];

		if(!player) throw new Error('err');

		player.registerRequest(req, res);
	}

	cellClick(req: Request, res: Response){
		const game = this.gamesList[(req.query as any).gameHash];
		game.cellClick(req, res);
	}
}

export default new Store();
