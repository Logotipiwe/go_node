import Store from "./Store";

const express = require('express');
const router = express.Router();

router.get('/create', (req, res) => {
	const q = req.query;
	if (typeof q.playerName !== 'string' || typeof q.deckSize !== "string") throw Error('strings required');
	const playerName: string = q.playerName;
	const deckSize: number = parseInt(q.deckSize);

	res.json(Store.newGame(playerName, deckSize));
});

router.get('/join',
	Store.checkGameHash,
	Store.joinGame
);

router.get('/pooling',
	Store.checkGameHash,
	Store.checkPlayerHash,
	Store.poolRequest
);

router.get('/cellClick', [
	Store.checkGameHash,
	Store.checkPlayerHash,
	Store.cellClick
]);

export default router;
