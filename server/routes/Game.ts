import Deck from "../models/Deck";
import md5 from "md5";
import Player from "../models/Player";
import {Response, Request} from "express";
import Store from "./Store";

export default class Game {
	constructor(firstPlayerName: string, deckSize: number, hash: string) {
		this.deck = new Deck(deckSize);
		this.hash = hash;

		const playerHash = this.generatePlayerHash();
		const player = new Player(firstPlayerName, playerHash);
		this.turn = player;
		this.players = {
			[playerHash]: player
		};
	}

	turn: Player;
	hash: string;
	deck: Deck;
	players: { [key: string]: Player };
	maxPlayers: number = 2;

	join(req: Request, res: Response, next) {
		if (Object.keys(this.players).length >= this.maxPlayers) return next(new Error("max playerss"));
		if (typeof req.query.playerName !== "string") return next(new Error("player name type wrong"));

		const playerHash = (Store.test) ? Store.testHash : this.generatePlayerHash();
		const opponent = Object.values(this.players)[0];
		const player = new Player(req.query.playerName, playerHash);

		if (Store.test) {
			const deckSize = this.deck.cells.length;
			this.deck.cells[1][0].player = player;
			this.deck.cells[0][1].player = player;
			this.deck.cells[1][2].player = player;
			this.deck.cells[2][1].player = player;

			this.deck.cells[1][3].player = opponent;
			this.deck.cells[0][4].player = opponent;
			this.deck.cells[1][5].player = opponent;
			this.deck.cells[2][4].player = opponent;

			this.deck.cells[deckSize-3][0].player = player;
			this.deck.cells[deckSize-3][1].player = player;
			this.deck.cells[deckSize-2][2].player = player;
			this.deck.cells[deckSize-1][2].player = player;

			this.deck.cells[deckSize-1][0].player = opponent;
			this.deck.cells[deckSize-1][1].player = opponent;
			this.deck.cells[deckSize-2][1].player = opponent;

		}

		player.opponent = opponent;
		opponent.opponent = player;

		this.turn = player;

		player.registerRequest(req, res);

		this.players[playerHash] = player;
		const ans = {
			type: "secPlayerJoined",
			gameHash: this.hash,
			playerHash,
			payload: {
				players: {
					[player.opponent!.hash]: {
						name: player.opponent!.name,
					},
					[player.hash]: {
						name: player.name
					}
				},
				deck: this.deck.deckObj,
				playerTurnName: this.turn.name
			}
		};
		player.sendData(ans);
		opponent.sendData(ans);
	}

	cellClick(req: Request, res: Response) {
		if (req.query.playerHash !== this.turn.hash) {
			res.json({ans: 'not ur turn'});
			return;
		}
		const rowNum = parseInt((req.query as any).rowNum);
		const cellNum = parseInt((req.query as any).cellNum);
		const cell = this.deck.cells[rowNum][cellNum];

		if (cell.player) {
			res.json({ans: 'занято'});
			return;
		}
		if (cell.isEnemyCellsAround(this.turn) &&
			cell.checkIfRemove()) {
			res.json({ans: "незя, окружен"});
			return;
		}

		cell.player = this.turn;
		cell.cellsNear.forEach(cell => cell.removeCells());
		if (cell.player) cell.removeCells();

		if(Store.testIsChangeTurn) this.turn = this.turn.opponent!;

		for (const playerHash in this.players) {
			const player: Player = this.players[playerHash];
			player.sendData({
				payload: {
					deck: this.deck.deckObj,
					playerTurnName: this.turn.name

				}
			});
		}
		res.json({ok: true});
	}

	generatePlayerHash() {
		return md5((Math.random() * 100000).toString());
	}
}
