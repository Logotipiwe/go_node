import Cell from "./Cell";

export default class Deck {
	constructor(size: number) {
		this.cells = new Array(size).fill(
			new Array(size).fill('')
		);
		this.cells = this.cells.map(row => row.map(() => new Cell()));
		for (let row = 0; row < size; row++) {
			for (let col = 0; col < size; col++) {
				const cell = this.cells[row][col];
				if (row > 0) cell.cellsNear.push(this.cells[row - 1][col]);
				if (col < size-1) cell.cellsNear.push(this.cells[row][col+1]);
				if (row < size-1) cell.cellsNear.push(this.cells[row+1][col]);
				if (col > 0) cell.cellsNear.push(this.cells[row][col-1]);
			}
		}
	}

	cells: Cell[][];

	get deckObj() {
		return this.cells.map(row => row.map(cell => {
			return {
				player: (cell.player) ? cell.player.hash : cell.player
			}
		}))
	}
};
