import Player from "./Player";

class Cell {
	player: null|Player = null;
	cellsNear: this[] = [];
	static checkedCells: Cell[] = [];

	removeCells(){
		const check = this.checkIfRemove();
		if(check){
			Cell.checkedCells.forEach(cell=>{
				cell.player = null;
			});
		}
		Cell.checkedCells = [];
	}

	checkIfRemove(): boolean{
		Cell.checkedCells.push(this);
		const cellAns = !this.isEmptyCellsNear;
		const cellsNearAns = this.cellsNearToCheck.map((cell)=>cell.checkIfRemove());
		const isRemove = [cellAns, ...cellsNearAns.flat()].every(ans=>ans);
		return isRemove;
	}

	get isEmptyCellsNear(): boolean{
		return !!this.cellsNear.filter(cell => (!cell.player)).length;
	}

	get cellsNearToCheck(): this[]{
		return this.cellsNear.filter(cell=>(cell.player === this.player && !Cell.checkedCells.includes(cell)));
	}

	isEnemyCellsAround(turn: Player){
		const enemyCells = this.cellsNear
			.filter(cell=>
			(cell.player && cell.player !== turn)
		);
		return this.cellsNear.length === enemyCells.length;
	}
}
export default Cell;
