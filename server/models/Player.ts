import {Request, Response} from "express";

export default class Player {
	constructor(name: string, hash: string) {
		this.name = name;
		this.hash = hash;

		this.sendData = this.sendData.bind(this);
		this.registerRequest = this.registerRequest.bind(this);
	}
	req: undefined|Request;
	res: undefined|Response;
	hash: string;
	dataToSend: object[] = [];
	opponent: undefined|this;

	name: string;
	hp: number = 100;

	sendData(data: object){
		if(this.res){
			this.res.json(data);
			this.res = undefined;
			this.req = undefined;
		} else {
			this.dataToSend.push(data);
		}
	}

	registerRequest(req: Request, res: Response){
		if(this.dataToSend.length){
			res.json(this.dataToSend.shift());
			return;
		}

		this.res = res;
		this.req = req;
	}
}
