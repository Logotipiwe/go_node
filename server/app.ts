import gameRouter from './routes/gameLifecycle'
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require("cors");

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(cors());

app.use('/', express.static('./public'));
app.use('/game', gameRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404));
});

app.use(function (err, req, res, next) {
	const message = err.message;
	const stack = err.stack;
	const json = {
		message, stack
	};
	res.status(err.status || 500);
	res.json(json)
});

export default app;
